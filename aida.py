import json
import os
import logging
import re
from time import sleep

import requests

logger = logging.getLogger("AP")
sh = logging.StreamHandler()
formatter = logging.Formatter(
    '[%(asctime)s][%(levelname)s][%(name)s]: %(message)s')
sh.setFormatter(formatter)
logger.addHandler(sh)
logger.setLevel(logging.DEBUG)


def parse_lines(data):
    values = []
    for i, line in enumerate(data.splitlines()):
        print(values)
        groups = re.split(r"(.*):", line)
        if len(groups) == 1:
            print(line)
            values[-1]['text'] = values[-1]['text'] + "\n" + line
        elif len(groups) == 3:
            values.append({"name": groups[1], "text": groups[2].strip()})
    return values


def parse_sub_groups(data):
    splitted = re.split(r"<< (.*) >>\n", data)[1:]
    info = []
    for name, data in list(zip(splitted[::2], splitted[1::2]))[:3]:
        group = {"name": name, 'values': parse_lines(data)}
        info.append(group)
    return info


def main():
    text = open("data.txt", "r").read()
    logger.info("Loaded data, size %s", len(text))
    splitted = re.split(r"<<< (.*) >>>\n\n", text)[1:]
    info = []
    for name, data in list(zip(splitted[::2], splitted[1::2])):
        logger.info("Parsing category %s", name)
        if re.search(r"<< (.*) >>\n", data):
            group = {"name": name, 'values': parse_sub_groups(data)}
        else:
            group = {"name": name, 'values': parse_lines(data)}
        info.append(group)
    with open("test.json", "w") as output:
        output.write(json.dumps(info, indent=4))


if __name__ == '__main__':
    main()
