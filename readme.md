# Installation

Installing Node.Js and NPM

```bash
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
source ~/.bashrc
nvm install node
```

Installing Android SDK for working with Appium

```bash
sudo apt install libstdc++6:i386 lib32z1 expect openjdk-8-jre openjdk-8-jdk
sudo apt remove openjdk-11-jre openjdk-11-jdk
mkdir $HOME/lib
cd $HOME/lib
wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
unzip sdk-tools-linux-4333796.zip -d android-sdk
cd ./android-sdk
printf "\n\n# Android SDK params\nexport ANDROID_HOME=$(pwd)\nexport PATH=\$PATH:\$ANDROID_HOME/tools/bin:\$ANDROID_HOME/platform-tools:\$ANDROID_HOME/build-tools/28.0.3\n" >> ~/.bashrc
printf "\nexport JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64\n" >> ~/.bashrc
source ~/.bashrc
sdkmanager "tools" "platforms;android-28" "platform-tools" "build-tools;28.0.3"
```

Install udev rules:
https://github.com/M0Rf30/android-udev-rules

Installing and starting Appium

```bash
npm install
npm start
```